# Travail pratique ― Todo List

## Objectif

Développer une application web locale, à l'aide de mes connaissances JavaScript, permettant de gérer une *ToDo List*.
Chacune des tâches peut-être cochée lorsqu'elle est terminée.

1. En m'appuyant sur mes connaissances JavaScript, je développe la fonctionnalité permettant, en cliquant sur un bouton, d'ajouter un élément à une liste de tâches. Cette tâche pourra être cochée afin d'être marquée comme terminée.
2. Pour chacune des tâches, j'ajoute un menu me permettant de sélectionner une priorité.
3. Pour chacune des tâches, je peux modifier son libellé ou supprimer celle-ci.
4. J'utilise les cookies afin de gérer la persistance des données et ainsi sauvegarder mes taches. 