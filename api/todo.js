/** 
 * actions des boutons 
 */
document.querySelector('#editTitle').addEventListener('click',manageTitle);
document.querySelector('#saveTitle').addEventListener('click',manageTitle);

function manageTitle(){

    var title = document.querySelector('#title');
    title.toggleAttribute('contenteditable');
    title.focus();

    document.querySelector('#editTitle').classList.toggle('hidden');
    document.querySelector('#saveTitle').classList.toggle('hidden');
    
}

/**
 * Niveau 1 : En m'appuyant sur mes connaissances JavaScript, je développe la fonctionnalité permettant, en cliquant sur un bouton, d'ajouter un élément à une liste de tâches. Cette tâche pourra être cochée afin d'être marquée comme terminée.
 * Niveau 2: Pour chacune des tâches, j'ajoute un menu me permettant de sélectionner une priorité.
 */

document.querySelector('#addTask').addEventListener('click',addTask);

function addTask(){
    alert('Ajouter une tâche');
    var nomTache = prompt("Nom de la tache :");
    if (nomTache === null) {
        return;
    }
    var itemTasks = document.createElement('li');
    itemTasks.className = 'tasks__item';
    var num = document.querySelectorAll('.tasks__item').length
    itemTasks.id = 'task-' + num;
    itemTasks.innerHTML = ` <input type="checkbox" name="task-${num}" id="task-checkbox-${num}">
                            <label for="task-checkbox-${num}" class="pls"><em class="placeholder">${nomTache}</em></label>
                            <button id="editTitle-${num}" style="margin-left: 10px" onclick="manageName('${num}')"><span class="icon-pencil"></span></button>
                            <button id="saveTitle-${num}" style="margin-left: 10px" onclick="manageName('${num}')" class="hidden"><span class="icon-floppy" aria-hidden="true"></span></button>
                            <button onclick="deleteTask('${num}')"><span class="icon-trash-1"></span></button>
                            <div style="margin-left: auto;">Priorité :
                            <input type="radio" id="basse-${num}" name="priorite-${num}" value="Basse" checked>
                            <label for="basse-${num}">Basse</label>
                            <input type="radio" id="moyenne-${num}" name="priorite-${num}" value="Moyenne">
                            <label for="moyenne-${num}">Moyenne</label>
                            <input type="radio" id="haute-${num}" name="priorite-${num}" value="Haute">
                            <label for="haute-${num}">Haute</label>
                            </div>`;
    document.querySelector('.tasks').appendChild(itemTasks);
}

/**
 * Niveau 3 : Pour chacune des tâches, je peux modifier son libellé ou supprimer celle-ci.
 */

function manageName(Id) {
    var taskLabel = document.querySelector(`#task-${Id} .pls em`);
    taskLabel.toggleAttribute('contenteditable');
    taskLabel.focus();

    document.querySelector('#editTitle-' + Id).classList.toggle('hidden');
    document.querySelector('#saveTitle-' + Id).classList.toggle('hidden');
}

function deleteTask(Id) {
    var taskToRemove = document.getElementById('task-' + Id);
    taskToRemove.parentNode.removeChild(taskToRemove);

    var tasks = document.querySelectorAll('.tasks__item');
    for (var i = Id + 1; i < tasks.length; i++) {
        var currentTask = tasks[i];
        currentTask.id = 'task-' + (i - 1);
        currentTask.querySelector('input[name^="task-"]').setAttribute('name', 'task-' + (i - 1));
        currentTask.querySelector('input[id^="task-checkbox-"]').setAttribute('id', 'task-checkbox-' + (i - 1));
        currentTask.querySelector('label[for^="task-checkbox-"]').setAttribute('for', 'task-checkbox-' + (i - 1));
        currentTask.querySelector('button[id^="editTitle-"]').setAttribute('id', 'editTitle-' + (i - 1));
        currentTask.querySelector('button[id^="saveTitle-"]').setAttribute('id', 'saveTitle-' + (i - 1));
        currentTask.querySelector('input[name^="priorite-"]').setAttribute('name', 'priorite-' + (i - 1));
        currentTask.querySelector('input[id^="basse-"]').setAttribute('id', 'basse-' + (i - 1));
        currentTask.querySelector('input[id^="moyenne-"]').setAttribute('id', 'moyenne-' + (i - 1));
        currentTask.querySelector('input[id^="haute-"]').setAttribute('id', 'haute-' + (i - 1));
        currentTask.querySelector('label[for^="basse-"]').setAttribute('for', 'basse-' + (i - 1));
        currentTask.querySelector('label[for^="moyenne-"]').setAttribute('for', 'moyenne-' + (i - 1));
        currentTask.querySelector('label[for^="haute-"]').setAttribute('for', 'haute-' + (i - 1));
    }
}

/**
 * Niveau 4 : J'utilise les cookies afin de gérer la persistance des données et ainsi sauvegarder mes taches
 */